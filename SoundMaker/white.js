Vex.Flow.WhiteSpace = function() { this.init(); }

Vex.Flow.WhiteSpace.prototype = new Vex.Flow.Note();
Vex.Flow.WhiteSpace.constructor = Vex.Flow.WhiteSpace;
Vex.Flow.WhiteSpace.superclass = Vex.Flow.Note.prototype;

Vex.Flow.WhiteSpace.prototype.init = function(x) {
    var superClass = Vex.Flow.WhiteSpace.superclass;
    superClass.init.call(this, {duration: "b"});

    this.metrics = { width: 8 };
    this.ignore_ticks = false;
    this.setWidth(this.metrics.width);
}

Vex.Flow.WhiteSpace.prototype.setStave = function(stave) {
    var superclass = Vex.Flow.WhiteSpace.superclass;
    superclass.setStave.call(this, stave);
}

Vex.Flow.WhiteSpace.prototype.getBoundingBox = function() {
    return new Vex.Flow.BoundingBox(this.getAbsoluteX(), 8, 8, 11);
}

Vex.Flow.WhiteSpace.prototype.preFormat = function() {
    this.modifierContext.preFormat();
    this.setPreFormatted(true);
    return this;
}

Vex.Flow.WhiteSpace.prototype.draw = function(stave, x) {
}

