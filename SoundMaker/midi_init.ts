/// <reference path="WebMIDIAPI.ts"/>

function onMIDIFailure(msg : string) : void {
    console.log("Failed to get MIDI access - " + msg);
}

interface Navigator {
    requestMIDIAccess(onMIDISuccess, onMIDIFailure);
}

function initMidi(continuation : (MIDIAccess) => any) : any {
    return navigator.requestMIDIAccess(((midi) =>
                continuation(midi)), onMIDIFailure);
}

function enumerateInputsAndOutputs(midiAccess : MIDIAccess) : void {
    var inputs = midiAccess.getInputs();
    var outputs = midiAccess.getOutputs();
    var i;

    var inList = document.getElementById("MidiIn");

    for (i = 0; i < inputs.length; i++) {
        inList.innerHTML += "<option>"
              + inputs[i].type + " port #" + i
              + " " + inputs[i].name
              + "</option>";
    }

    var outList = document.getElementById('MidiOut');

    for (i = 0; i < outputs.length; i++) {
        outList.innerHTML += "<option>" +
              outputs[i].type + " port #" + i +
              " " + outputs[i].name
              + "</option>";
    }
}
