/// <reference path="midi_init.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="WebMIDIAPI.d.ts" />
/// <reference path="miditrack.ts" />
/// <reference path="gui.ts" />
/// <reference path="song.ts" />
/// <reference path="keymode.ts" />

declare var Vex: any;

function GetPos(ev, div) {
    return {
        x: ev.pageX - $(div).position().left,
        y: ev.pageY - $(div).position().top
    };
}

function IntToCol(i) {
    var col = i.toString(16);
    return"#" + Array(7 - col.length).join('0') + col;
}

function ColToInt(r, g, b) {
    return b + g * 256 + r * 256 * 256;
}

window.onload = () => {
    var song = new Song();
    var player;

    var canvas = <HTMLCanvasElement>document.getElementById('sheet');

    var sheet = new GuiSheet(canvas, song);

    var gui = new Gui;
    initMidi((midi) => {
            player = new Player(midi);
            enumerateInputsAndOutputs(midi);
            var i = midi.getInput(0);
            //i.onmessage = (ev) => o.send(ev.data);
            $('#MidiOut').change((ev) =>
                player.changeOutput($('#MidiOut').prop('selectedIndex')));
            $('#MidiIn').change((ev) =>
                player.changeInput($('#MidiIn').prop('selectedIndex')));
            player.tempo = 4000;
        });

    document.getElementById('playsong').onclick = ((ev) => {
        player && player.playSong(song, song.cursor.bar);
    });

    $('#instrument').change((ev) => {
        var instr = $('#instrument').prop('selectedIndex');
        player && player.changeInstrument(instr,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        song.tracks[song.cursor.track].instrument = instr;
    });

    sheet.AddStave('treble', 0, 10, 600);
    sheet.Draw();

    canvas.onclick = (e) => {
            var pos = GetPos(e, canvas);
            sheet.SelectAt(pos.x, pos.y);
        };

    var keymap = keymaps.Vim;
    $('body').keydown((ev) => {
        if (ev.keyCode == keymap.right)
            sheet.CursorGo(0);
        else if (ev.keyCode == keymap.left)
            sheet.CursorGo(1);
        else if (ev.keyCode == keymap.up)
            sheet.CursorGo(2);
        else if (ev.keyCode == keymap.down)
            sheet.CursorGo(3);
        else if (ev.keyCode == keymap.insert) {
            var n = sheet.InsertNote();
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else if (ev.keyCode == keymap.increment) {
            var n = sheet.IncDecDuration(1);
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else if (ev.keyCode == keymap.decrement) {
            var n = sheet.IncDecDuration(-1);
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else if (ev.keyCode == keymap.delnote)
            sheet.DelNote();
        else if (ev.keyCode == keymap.delkey)
            sheet.DelKey();
        else if (ev.keyCode == keymap.upkey) {
            var n = sheet.IncDecKey(1);
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else if (ev.keyCode == keymap.downkey) {
            var n = sheet.IncDecKey(-1);
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else if (ev.keyCode == keymap.insertafter) {
            var n = sheet.InsertNoteAfter();
            player && player.playNote(n,
                sheet.song.tracks[sheet.song.cursor.track].channel);
        } else
            console.log('key: ' + ev.keyCode);
    });

    $('#scroll').change((ev) => sheet.Draw());

    $('#addtrack').click((ev) => {
            var name = prompt('Enter a name for your track:');
            sheet.AddTrack(name);
            gui.RefreshSongView(sheet.song);

        });

    $('#trackselect').change((ev) => {
            sheet.song.cursor.track = $('#trackselect').prop('selectedIndex');
            sheet.song.cursor.note = 0;
            sheet.Draw();
            gui.RefreshSongView(song);
        });

    $('#tempo').change((ev) => {
            if (player)
                player.tempo = 60 / $('#tempo').val() * 4000;
        });

    gui.RefreshSongView(song);
    gui.CreateInstrumentsList(
            <HTMLSelectElement>document.getElementById('instrument'));
};

