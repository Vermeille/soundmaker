﻿/// <reference path="WebMIDIAPI.d.ts" />
/// <reference path="song.ts" />

class Signature {
    duration: number;
    base: number;

    constructor(duration: number, base: number) {
        this.duration = duration;
        this.base = base;
    }

    toScalar(): number {
        return this.duration / this.base;
    }

    static add(s1: Signature, s2: Signature): Signature {
        return new Signature(s1.duration * s2.base + s2.duration * s1.base,
                s1.base * s2.base);
    }
}

enum EventType {
    MIDI
}

interface SdMidiEvent {
    port: number;
    type: EventType;
    duration: Signature;
}

class Player {
    _midi: MIDIAccess;
    _output: MIDIOutput;
    _input: MIDIInput;
    tempo: number;

    constructor(midi: MIDIAccess) {
        this._midi = midi;
        this._output = midi.getOutput(0);
        this._input = midi.getInput(0);
    }

    changeOutput(n: number): void {
        this._output = this._midi.getOutput(n);
    }

    changeInput(n: number): void {
        this._input = this._midi.getInput(n);
    }

    changeInstrument(instr: number, channel: number): void {
        this._output.send([0xC0 + channel, instr]);
    }

    startNote(note: number, velocity: number, channel: number) {
        this._output.send([0x90 + channel, note, velocity]);
    }

    stopNote(note: number, velocity: number, channel: number, delay: number = 0) {
        if (delay == 0)
            this._output.send([0x80 + channel, note, velocity]);
        else
            this._output.send([0x80 + channel, note, velocity],
                    window.performance.now() + delay);
    }

    playSong(song: Song, bar: number) {
        for (var i = 0; i < song.tracks.length; ++i) {
            this.playTrack(song.tracks[i], bar, 0);
        }
    }

    playNote(note: Note, channel: number) {
        for (var i = 0; i < note.keys.length; ++i) {
            var tone = note.keys[i];
            console.log('plays: ' + tone.toString());
            this.startNote(tone, 0x7F, channel);
            this.stopNote(tone, 0x7F, channel, (1 / note.duration) * this.tempo);
        }
    }

    playTrack(track: Track, bar: number, note: number) {
        if (bar == track.bars.length)
            return;

        if (note == track.bars[bar].notes.length) {
            this.playTrack(track, bar + 1, 0);
            return;
        }

        var toPlay = track.bars[bar].notes[note];

        if (toPlay == null) {
            this.playTrack(track, bar, note + 1);
            return;
        }
        this.playNote(toPlay, track.channel);

        window.setTimeout(() => this.playTrack(track, bar, note + 1),
            (1 / toPlay.duration) * this.tempo);
    }
}
