﻿/// <reference path="miditrack.ts" />
/// <reference path="instruments.ts" />
/// <reference path="jquery.d.ts" />
/// <reference path="song.ts" />

declare var Vex: any;

var note_to_line = [0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 7];
var line_to_note = [0, 2, 4, 5, 7, 9, 11];

class GuiSheet {
    public song: Song;
    private canvas: HTMLCanvasElement;
    private renderer: any;
    private sheet_ctx: CanvasRenderingContext2D;
    private staves: any[];
    private picker: HTMLCanvasElement;
    private pickctx: CanvasRenderingContext2D;
    private col: number; // keeps track of offscreen color

    constructor(canvas, song) {
        this.canvas = canvas;
        this.renderer
            = new Vex.Flow.Renderer(canvas, Vex.Flow.Renderer.Backends.CANVAS);
        this.sheet_ctx = this.renderer.getContext();
        this.staves = [];
        this.picker = <HTMLCanvasElement>document.getElementById('clickme');
        this.pickctx = this.picker.getContext('2d');
        this.song = song;
    }

    AddStave(clef: string, x: number, y: number, w: number): any {
        var stave = new Vex.Flow.Stave(x, y, w);
        stave.addClef(clef).setContext(this.sheet_ctx);
        stave.addTimeSignature('C');
        this.staves.push(stave);
        return this.staves[this.staves.length - 1];
    }

    FetchTrack(t: number): any {
        var notes_count = 0;
        var lines = [];
        var notes = [];
        var track = this.song.tracks[t];
        for (var b = 0; b < track.bars.length - 1; ++b) {
            notes_count += track.bars[b].notes.length;
            for (var i = 0; i < track.bars[b].notes.length; ++i) {
                var n = track.bars[b].notes[i];

                if (n)
                    notes.push(n.ToVex());
                else
                    notes.push(new Vex.Flow.WhiteSpace());
            }
            if (notes_count < 16)
                notes.push(new Vex.Flow.BarNote(Vex.Flow.Barline.SINGLE));
            else {
                notes_count = 0;
                lines.push(notes);
                notes = [];
            }
        }
        //notes.push(new Vex.Flow.StaveNote({keys: ["B/4"], duration: "qr" }));
        notes.push(new Vex.Flow.WhiteSpace());
        lines.push(notes);
        return lines;
    }

    Draw() {
        // clear the sheet
        this.sheet_ctx.fillStyle = "#FFFFFF";
        this.sheet_ctx.fillRect(0, 0, this.canvas.width - 1,
                this.canvas.height - 1);

        // clear the offscreen buffer
        this.pickctx.fillStyle = "#FFFFFF";
        this.pickctx.fillRect(0, 0, this.picker.width - 1,
                this.picker.height - 1);

        this.sheet_ctx.fillStyle = "#000000";

        var lines = this.FetchTrack(this.song.cursor.track);

        this.staves = [];

        var scroll = $('#scroll').val();
        this.col = 0;
        for (var i = 0; i < lines.length; ++i) {
            var stave = this.AddStave('treble', 0,
                    100 * i - scroll * ((lines.length - 1) * 100) / 100, 1000);
            this.DrawStave(stave, lines[i]);
        }

        var curx = 0;
        var bar = 0;
        var line = 0;
        for (i = 0; bar != this.song.cursor.bar; ++i) {
            if (i == lines[line].length) {
                ++line;
                i = -1;
                ++bar;
            }
            if (lines[line][i] instanceof Vex.Flow.BarNote)
                ++bar;
        }

        i += this.song.cursor.note;

        // draw cur
        var curpos = {
            x: lines[line][i].getBoundingBox().x,
            y: this.staves[line].getYForNote(Math.floor((this.song.cursor.key - 48) / 12)
                    * 3.5 + note_to_line[this.song.cursor.key % 12] / 2) - 5
        };
        this.sheet_ctx.fillStyle = '#FF0000';
        this.sheet_ctx.fillRect(curpos.x, curpos.y, 12, 8);
    }

    DrawStave(stave, notes: any[]): void {
        stave.draw();
        Vex.Flow.Formatter.FormatAndDraw(this.sheet_ctx, stave, notes);

        // draw it
        for (var t = 0; t < notes.length; ++t) {
            if (!(notes[t] instanceof Vex.Flow.BarNote)) {
                var bb = notes[t].getBoundingBox();
                for (var i = -5; i < 15; ++i) {
                    this.pickctx.fillStyle = IntToCol(this.col++);
                    this.pickctx.fillRect(bb.x - 6,
                            stave.getYForNote(i / 2), bb.w + 7, 7);
                }
            } else {
                this.col = (Math.floor(this.col / 2000) + 1) * 2000;
            }
        }
        this.col = (Math.floor(this.col / 2000) + 1) * 2000;
    }

    SelectAt(x: number, y: number): void {
        var col = this.pickctx.getImageData(x, y, 1, 1).data;
        var idx = ColToInt(col[0], col[1], col[2]);

        if (idx == 0xFFFFFF)
            return;

        console.log('color: ' + idx);

        this.song.cursor.bar = Math.floor(idx / 2000);
        this.song.cursor.note = Math.floor((idx % 2000) / 20);
        this.song.cursor.key = 36 + line_to_note[(Math.abs(idx + 1) % 20) % 7]
                + Math.floor(((idx + 1) % 20) / 7) * 12;
        console.dir(this.song.cursor);
        this.Draw();
    }

    CursorGo(direc: number) {
        var cur = this.song.cursor;
        var line = note_to_line[cur.key % 12];

        if (direc == 0 || direc == 1)
            this.song.CursorGo(direc);
        else if (direc == 2 && cur.key < 88) {
            cur.key = line_to_note[(line + 1) % 7]
                + 12 * Math.floor((line + 1) / 7)
                + Math.floor(cur.key / 12) * 12;
        } else if (direc == 3 && cur.key > 0) {
            cur.key = line_to_note[(line - 1 + 7) % 7]
                + 12 * Math.floor((line - 1) / 7)
                + Math.floor(cur.key / 12) * 12;
        }
        this.Draw();
    }

    InsertNote(): Note {
        var note = this.song.InsertNote();
        this.Draw();
        return note;
    }

    InsertNoteAfter(): Note {
        var note = this.song.InsertNoteAfter();
        this.Draw();
        return note;
    }

    IncDecDuration(incdec: number): Note {
        var note = this.song.IncDecDuration(incdec);
        this.Draw();
        return note;
    }

    DelNote() {
        this.song.DelNote();
        this.Draw();
    }

    DelKey() {
        this.song.DelKey();
        this.Draw();
    }

    IncDecKey(incdec: number): Note {
        var note = this.song.IncDecKey(incdec);
        this.Draw();
        return note;
    }

    AddTrack(name: string): void {
        this.song.AddTrack(name);
        this.song.cursor.track = this.song.tracks.length - 1;
        this.song.cursor.bar = 0;
        this.song.cursor.note = 0;
        this.Draw();
    }
}

class Gui {
    constructor() { }
    CreateInstrumentsList(div: HTMLSelectElement): void {
        div.innerHTML = "";
        for (var i = 0; i < instruments.length; ++i) {
            div.innerHTML += '<option>' + instruments[i] + '</option>';
        }
    }

    RefreshSongView(s: Song): void {
        $('#trackselect').html('');
        for (var i = 0; i < s.tracks.length; ++i)
            $('#trackselect').append('<option>' + s.tracks[i].name
                    + '</option>');

        $('#trackselect').prop('selectedIndex', s.cursor.track);
        $('#instrument').prop('selectedIndex',
                s.tracks[s.cursor.track].instrument);
    }
}


