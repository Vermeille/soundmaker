﻿interface MIDIAccess {
    getInputs(): MIDIPort[];
    getOutputs(): MIDIPort[];
    getInput(i : number) : MIDIInput;
    getOutput(i : number) : MIDIOutput;
}

interface MIDIPort {
    type : string;
    name : string;
    manufacturer : string;
    version : string;
    fingerprint : string;
    toString() : string;
}

interface MIDIInput {
    onmessage : (msg : any) => any;
    addEventListener : any;
    removeEventListener : any;
    preventDefault() : void;
    dispatchEvent(ev : any) : any;
}

interface MIDIOutput {
    constructor(midi : MIDIAccess, target : number);
    send(data : Array, timestamp? : number) : bool;
}