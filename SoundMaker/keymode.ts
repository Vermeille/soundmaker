var keymaps = {
    Normal: {
        left: 37,
        down:38,
        up: 40,
        right: 39,
        insert: 13, // enter
        increment: 107, // +
        decrement: 109, // -
        delnote: 88, // x
        delkey: 46, // suppr

    },
    Vim: {
        left: 72,
        down:74,
        up: 75,
        right: 76,
        insert: 73, // i
        increment: 68, // d
        decrement: 81, // q
        delnote: 88, // x
        delkey: 67, // c
        upkey: 90, // z
        downkey: 83, // s
        insertafter: 65
    }
};
