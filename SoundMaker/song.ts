/// <reference path="miditrack.ts" />
/// <reference path="array.ts" />

var midi_to_note: string[] = [ "C", "C#", "D", "D#", "E",
                     "F", "F#", "G", "G#", "A", "A#", "B" ]

class Note {
    keys: number[];
    duration: number;

    constructor(tones: number[], duration: number) {
        this.keys = tones;
        this.duration = duration;
    }

    ToVex(): any {
        var ks = [];
        this.keys.sort();
        ks.sort();
        for (var i = 0; i < this.keys.length; ++i) {
            var k = midi_to_note[Math.floor(this.keys[i]) % 12];
            k += "/" + Math.floor(this.keys[i] / 12);
            ks.push(k);
        }
       var vex = new Vex.Flow.StaveNote({ keys: ks, duration:
                this.duration.toString() });

       for (var i = 0; i < this.keys.length; ++i) {
           if (midi_to_note[this.keys[i] % 12].length > 1
                   && midi_to_note[this.keys[i] % 12][1] == "#") {
               vex.addAccidental(i, new Vex.Flow.Accidental('#'));
           }
       }

       return vex;
    }
}

class Bar {
    notes: Note[];

    constructor() {
        this.notes = [null];
    }
}

class Track {
    bars: Bar[];
    channel: number;
    name: string;
    instrument: number;

    constructor(channel: number, name: string) {
        this.bars = [new Bar()];
        this.channel = channel;
        this.name = name;
        this.instrument = 0;
    }
}

class Song {
    tracks: Track[];
    cursor: Cursor;

    constructor() {
        this.cursor = new Cursor();
        this.tracks = [new Track(0, "My Piano")];
    }

    CursorGo(direc: number) {
        this.cursor.Go(this, direc);
    }

    AppendBar(): void {
        for (var t = 0; t < this.tracks.length; ++t) {
            this.tracks[t].bars.push(new Bar());
        }
    }

    AddTrack(tname: string): void {
        var bars = [];
        for (var b = 0; b < this.tracks[0].bars.length; ++b) {
            bars.push(new Bar());
        }
        this.tracks.push(new Track(this.tracks.length, tname));
        this.tracks[this.tracks.length - 1].bars = bars;
    }

    InsertNote(): Note {
        var cur = this.cursor;
        var track = this.tracks[cur.track];
        var bar = track.bars[cur.bar];

        // Last bar
        if (cur.bar == track.bars.length - 1) {
            this.AppendBar();
            var note = new Note([cur.key], 4);
            bar = track.bars[cur.bar];
            bar.notes = [note, null];
            return note;
        // Last note of an incomplete bar
        } else if (!bar.notes[cur.note]) {
            bar.notes[cur.note] = new Note([cur.key], 4);
            bar.notes.push(null);
            return bar.notes[cur.note];
        } else {
            bar.notes[cur.note].keys.push(cur.key);
            return bar.notes[this.cursor.note];
        }
    }

    InsertNoteAfter(): Note {
        var cur = this.cursor;
        var track = this.tracks[cur.track];
        var bar = track.bars[cur.bar];

        var note = new Note([cur.key], 4);
        ++this.cursor.note;
        bar.notes.insert(cur.note, note);
        return note;
    }

    IncDecDuration(incdec: number) {
        var cur = this.cursor;
        var n = this.tracks[cur.track].bars[cur.bar].notes[cur.note];

        if (incdec == -1 && n.duration > 0.125)
            n.duration *= 0.5;
        else if (incdec == 1 && n.duration < 64)
            n.duration *= 2;
        return n;
    }

    DelNote() {
        var cur = this.cursor;
        var bar = this.tracks[cur.track].bars[cur.bar];

        if (bar.notes[cur.note] == null)
                return;

        if (bar.notes.length > 1)
            bar.notes.removeAt(cur.note);
        if (bar.notes.length >= cur.note)
            cur.note = bar.notes.length - 1;
    }

    DelKey() {
        var cur = this.cursor;
        var note = this.tracks[cur.track].bars[cur.bar].notes[cur.note];

        if (note == null)
                return;

        if (note.keys.length == 1)
            this.DelNote();
        else {
            var idx = this.FindUnderCursor();
            if (idx != -1)
                note.keys.splice(idx, 1);
        }
    }

    IncDecKey(incdec: number): Note {
        var cur = this.cursor;
        var note = this.tracks[cur.track].bars[cur.bar].notes[cur.note];
        var idx = this.FindUnderCursor();

        if (idx != -1) {
            if (incdec == 1)
                ++note.keys[idx];
            else
                --note.keys[idx];
            this.cursor.key = note.keys[idx];
            return note;
        }

        return null;
    }

    private FindUnderCursor(): number {
        var cur = this.cursor;
        var note = this.tracks[cur.track].bars[cur.bar].notes[cur.note];
        var idx = -1;

        for (var i = 0; i < note.keys.length; ++i) {
            if (note_to_line[note.keys[i] % 12]
                    == note_to_line[this.cursor.key % 12]
                    && Math.floor(note.keys[i] / 12)
                    == Math.floor(this.cursor.key / 12)) {
                idx = i;
                break;
            }
        }
        return idx;
    }
}

class Cursor {
    track: number;
    bar: number;
    note: number;
    key: number;

    constructor() {
        this.track = 0;
        this.bar = 0;
        this.note = 0;
        this.key = 48;
    }

    GoRight(s: Song): bool {
        if (this.note + 1 >= s.tracks[this.track].bars[this.bar].notes.length)
            return this.GoNextBar(s);
        else {
            ++this.note;
            return true;
        }
    }

    GoLeft(s: Song): bool {
        if (this.note == 0) {
            if (this.bar > 0) {
                this.GoPrevBar(s);
                this.note = s.tracks[this.track].bars[this.bar].notes.length - 1;
                return true;
            }
            return false;
        } else {
            --this.note;
            return true;
        }
    }

    GoNextBar(s: Song): bool {
        if (this.bar < s.tracks[this.track].bars.length - 1) {
            this.note = 0;
            ++this.bar;
            return true;
        }
        return false;
    }

    GoPrevBar(s: Song): bool {
        if (this.bar > 0) {
            this.note = 0;
            --this.bar;
            return true;
        }
        return false;
    }

    Go(s: Song, direc: number): bool {
        if (direc == 0)
            return this.GoRight(s);
        else if (direc == 1)
            return this.GoLeft(s);
        else if (direc == 2 && this.key < 19)
            ++this.key;
        else if (direc == 3 && this.key > 0)
            --this.key;
        return true;
    }
}

