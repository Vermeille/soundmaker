interface Array {
    insert(idx: number, item: any): void;
    removeAt(idx: number): void;
}

Array.prototype.insert = function(idx, item) {
    this.splice(idx, 0, item);
}

Array.prototype.removeAt = function(idx) {
    this.splice(idx, 1);
}

